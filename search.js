const qs = require("querystring");
const fs = require("fs");
const fetch = require("node-fetch");
const parser = require("js-yaml");
const _ = require("lodash");
const inquirer = require('inquirer');
const helper = require('./script');


function writeResults(results) {
    fs.writeFileSync(
        "search-results.yml",
        parser.safeDump(results, { skipInvalid: true }),
        {
            encoding: "utf-8",
            flag: "w+",
        }
    );
}

async function search(term) {
    let body = qs.stringify({
        query: term,
        // 'facet[category]': 'true',
        // 'facet-limit[category]': '30',
        // 'facet-min[category]': '1',
        "filter[category]": "آموزش",
        // pr: 'true',
        // 'user-id': 'b956ad72694caaf99ae8038591061ce7',
        fields: "url,title,image,view_count,published_date",
        limit: "1000000",
    });

    const response = await fetch(
        `https://api.taxus.ir/v1/search/eyJhbGciOiJIUzUxMiJ9.eyJjIjoiYmNkZWRhYjAtMmMwOS00NjZiLWI3MzYtMTM0ZTU5YjYzMWZkIiwidSI6IjA2NjQ2Nzk5LWNmZWItNDI1ZS1iZTI5LTEyNTE0ZjU0MWUzNyJ9.P11myBUCipxXLZFcealm-aYqjRSxBSx7ddAcrOx25VnYf5nOILnFRIngfWAVz-0lWaZQQ67TYZU2PqPlJduFhA/?${body}`,
        {
            headers: {
                Host: "api.taxus.ir",
                Connection: "keep-alive",
                Pragma: "no-cache",
                "Cache-Control": "no-cache",
                Accept: "*/*",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36",
                Origin: "https://p30download.com",
                "Sec-Fetch-Site": "cross-site",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Dest": "empty",
                Referer: "https://api.taxus.ir/",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "en-US,en;q=0.9",
            },
            referrer: "https://api.taxus.ir/",
            referrerPolicy: "no-referrer-when-downgrade",
            body: null,
            method: "GET",
            mode: "cors",
        }
    );

    return await response.json();
}

async function searchTerms(terms) {
    const results = await Promise.all(_.uniq(terms.map(search)));
    return _.uniqBy(
        _.flatten(
            results.map((result) =>
                result.result.items.map((item) => ({
                    url: item.url,
                    title: item.title.slice(7, item.title.lastIndexOf(' - ')).trim(),
                }))
            )
        ),
        "url"
    );
}

function searchAndWriteResluts(terms) {
    searchTerms(terms)
        .then(filterResults)
        .then(links => helper.getLinks(...links))
        .then(helper.storeAsAria2cInputFile)
}

function filterResults(results) {
    return inquirer
        .prompt([
            {
                type: 'confirm',
                name: 'defaultSelected',
                message: 'Default to be selected?',
                default: false,
            },
        ])
        .then((answers) => {
            return inquirer
                .prompt([
                    {
                        type: 'checkbox',
                        name: 'urls',
                        pageSize: 30,
                        choices: results.map((item, i) => ({
                            name: `${i + 1}) ${item.title}`,
                            value: item.url,
                            checked: answers.defaultSelected,
                        })),
                    },
                ]);
        })
        .then(answers => answers.urls);
}

if (require.main === module) {
    searchAndWriteResluts(process.argv.slice(2));
}
