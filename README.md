# P30DL Wepbage Link Fetcher

A CLI application which scraps https://p30download.com post pages and finds download links, then it stores them in a file (`link.txt`) in `aria2c` input file format, grouped by their post title as the `aria2c` download destination directory.
