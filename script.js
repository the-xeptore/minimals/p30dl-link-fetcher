const fs = require('fs');
const axios = require('axios');
const cheerio = require('cheerio');
const _ = require('lodash');


async function getPageHTML(url) {
    const response = await axios.get(url);
    return response.data;
}

async function getPageLinks(url) {
    const html = await getPageHTML(url);
    const $ = cheerio.load(html);

    const links = $('aside>div>h3')
        .filter(function () { return $(this).text() === 'لینک های دانلود' })
        .siblings('p')
        .children('a')
        .map((_, a) => a.attribs)
        .toArray()
        .map((x) => x.href);

    const title = $('div>p>strong').filter(function() { return $(this).text() === 'نام انگلیسی:' })[0].next.nodeValue.trim();

    return { title, links };
}

async function getLinks(...urls) {
    return _.uniq(
        _.flattenDeep(
            await Promise.all(urls.map((url) => getPageLinks(url)))
        )
    );
}

function storeAsAria2cInputFile(courseLinks) {
    const file = fs.createWriteStream('links.txt');
    for (const course of courseLinks) {
        const links = course.links.map((link) => `${link}\n  dir=${course.title}`);
        file.write(links.join('\n') + '\n');
    }
}

if (require.main === module) {
    getLinks(...process.argv.slice(2))
        .then((result) => {
            storeAsAria2cInputFile(result);
        });
}

module.exports = {
    getLinks,
    storeAsAria2cInputFile,
};
